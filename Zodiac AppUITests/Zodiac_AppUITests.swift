//
//  Zodiac_AppUITests.swift
//  Zodiac AppUITests
//
//  Created by qiaoqiao peng on 4/15/18.
//  Copyright © 2018 LTU. All rights reserved.
//

import XCTest

class Zodiac_AppUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        
        let app = XCUIApplication()
        app.otherElements.containing(.navigationBar, identifier:"Zodiac_App.HoroscopesDetailView").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.tap()
        app.navigationBars["Horoscope"].tap()
        
        let tablesQuery = app.tables
        tablesQuery/*@START_MENU_TOKEN@*/.cells.containing(.staticText, identifier:"cancer")/*[[".cells.containing(.staticText, identifier:\"July\")",".cells.containing(.staticText, identifier:\"cancer\")"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.children(matching: .image).element.tap()
        app.navigationBars["Zodiac_App.HoroscopesDetailView"].buttons["Horoscope"].tap()
        tablesQuery/*@START_MENU_TOKEN@*/.cells.containing(.staticText, identifier:"leo")/*[[".cells.containing(.staticText, identifier:\"August\")",".cells.containing(.staticText, identifier:\"leo\")"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.children(matching: .image).element.tap()
        
                
        
      
    }
    
}
