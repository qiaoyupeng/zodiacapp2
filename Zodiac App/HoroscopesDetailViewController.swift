//
//  HoroscopesDetailViewController.swift
//  Zodiac App
//
//  Created by qiaoqiao peng on 3/26/18.
//  Copyright © 2018 LTU. All rights reserved.
//

import UIKit
import WebKit

class HoroscopesDetailViewController: UIViewController {

    var currentHoroscopesDetail:HoroscopeDetail?
    
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var horoscopeNameLabel: UILabel!
    @IBOutlet weak var horoscopeImage: UIImageView!
    @IBOutlet weak var horoscopeSymbolLabel: UILabel!
    @IBOutlet weak var horoscopeMonthLabel: UILabel!
    @IBOutlet weak var horoscopeDescriptionLabel: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        horoscopeNameLabel.text = currentHoroscopesDetail?.horoscopeName
        horoscopeSymbolLabel.text = currentHoroscopesDetail?.horoscopeSymbol
        horoscopeMonthLabel.text = currentHoroscopesDetail?.horoscopeMonth
        horoscopeDescriptionLabel.text = currentHoroscopesDetail?.horoscopeDescription
        horoscopeImage.image = UIImage(named: currentHoroscopesDetail!.fullImageName!)
        
        let htmlString = "<html><body><iframe style=\"position:absolute; top:0;left:0; width:100%; height:100%;\" src=\"\(currentHoroscopesDetail!.webSiteURL!)\" frameborder=\"0\" allowfullscreen></iframe></body></html>"
        
        webView.loadHTMLString(htmlString, baseURL: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
