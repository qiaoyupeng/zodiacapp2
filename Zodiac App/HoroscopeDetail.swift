//
//  HoroscopeDetail.swift
//  Zodiac App
//
//  Created by qiaoqiao peng on 3/26/18.
//  Copyright © 2018 LTU. All rights reserved.
//

import UIKit

class HoroscopeDetail: NSObject {
    
    var horoscopeName: String?
    var horoscopeDescription:String?
    var fullImageName:String?
    var thumbImageName:String?
    var horoscopeSymbol: String?
    var horoscopeMonth: String?
    var webSiteURL:String?

}
