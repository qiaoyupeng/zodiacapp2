//
//  MySplitViewController.swift
//  Zodiac App
//
//  Created by qiaoqiao peng on 4/4/18.
//  Copyright © 2018 LTU. All rights reserved.
//

import UIKit

class MySplitViewController: UISplitViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let leftNavController = self.viewControllers.first as!
        UINavigationController
        let masterViewController = leftNavController.topViewController as!
        HoroscopeTableViewController
        let detailViewController = self.viewControllers.last as!
        HoroscopesDetailViewController
        masterViewController.detailViewController = detailViewController

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
