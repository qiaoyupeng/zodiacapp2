//
//  HoroscopeTableViewController.swift
//  Zodiac App
//
//  Created by qiaoqiao peng on 3/26/18.
//  Copyright © 2018 LTU. All rights reserved.
//

import UIKit

class HoroscopeTableViewController: UITableViewController {
    
    let horoscopesModel = HoroscopesModel()
    
    var detailViewController: HoroscopesDetailViewController!

    override func viewDidLoad() {
        super.viewDidLoad()
        horoscopesModel.fetch()
        
    }
    
    func refreshView() {
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return horoscopesModel.horoscopeDetails.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "horoscopeCell", for: indexPath)

        // Configure the cell...
        let horoscopeDetail = horoscopesModel.horoscopeDetails[indexPath.row]
        cell.textLabel?.text = horoscopeDetail.horoscopeName
        cell.detailTextLabel?.text = horoscopeDetail.horoscopeMonth
        cell.imageView?.image = UIImage(named: horoscopeDetail.thumbImageName!)

        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 88
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath:
        IndexPath) {
        if (UIDevice.current.model.range(of: "iPad") != nil) {
            let bandDetail = horoscopesModel.horoscopeDetails[indexPath.row]
        }
    }

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if (segue.identifier == "showDetail") {
            
            if let indexPath = self.tableView.indexPathForSelectedRow {
                
                let horoscopesDetailViewController:HoroscopesDetailViewController = segue.destination as! HoroscopesDetailViewController
                let horoscopeDetail = horoscopesModel.horoscopeDetails[indexPath.row]
                horoscopesDetailViewController.currentHoroscopesDetail = horoscopeDetail
                
            }
            
        }

    }
    

}
