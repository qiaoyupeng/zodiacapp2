//
//  HoroscopesModel.swift
//  Zodiac App
//
//  Created by qiaoqiao peng on 4/4/18.
//  Copyright © 2018 LTU. All rights reserved.
//

import UIKit

class HoroscopesModel: NSObject {
    var horoscopeDetails = [HoroscopeDetail]()
    func fetch() {
        let AriesHoroscopeDetail = HoroscopeDetail()
        AriesHoroscopeDetail.horoscopeName = "Aries"
        AriesHoroscopeDetail.fullImageName = "full-aries"
        AriesHoroscopeDetail.thumbImageName = "aries"
        AriesHoroscopeDetail.horoscopeSymbol = "Ram"
        AriesHoroscopeDetail.horoscopeMonth = "April"
        AriesHoroscopeDetail.horoscopeDescription = "Courageous and Energetic"
        AriesHoroscopeDetail.webSiteURL = "https://www.astrology.com/horoscope/daily/aries.html"
        
        let TaurusHoroscopeDetail = HoroscopeDetail()
        TaurusHoroscopeDetail.horoscopeName = "taurus"
        TaurusHoroscopeDetail.fullImageName = "full-taurus"
        TaurusHoroscopeDetail.thumbImageName = "taurus"
        TaurusHoroscopeDetail.horoscopeSymbol = "Bull"
        TaurusHoroscopeDetail.horoscopeMonth = "May"
        TaurusHoroscopeDetail.horoscopeDescription = "Known for being reliable, practical, ambitious and sensual"
        TaurusHoroscopeDetail.webSiteURL = "https://www.astrology.com/horoscope/daily/taurus.html"
        
        let GeminiHoroscopeDetail = HoroscopeDetail()
        GeminiHoroscopeDetail.horoscopeName = "gemini"
        GeminiHoroscopeDetail.fullImageName = "full-gemini"
        GeminiHoroscopeDetail.thumbImageName = "gemini"
        GeminiHoroscopeDetail.horoscopeSymbol = "Twins"
        GeminiHoroscopeDetail.horoscopeMonth = "June"
        GeminiHoroscopeDetail.horoscopeDescription = "Gemini-born are clever and intellectual."
        GeminiHoroscopeDetail.webSiteURL = "https://www.astrology.com/horoscope/daily/gemini.html"
        
        let CancerHoroscopeDetail = HoroscopeDetail()
        CancerHoroscopeDetail.horoscopeName = "cancer"
        CancerHoroscopeDetail.fullImageName = "full-cancer"
        CancerHoroscopeDetail.thumbImageName = "cancer"
        CancerHoroscopeDetail.horoscopeSymbol = "Crab"
        CancerHoroscopeDetail.horoscopeMonth = "July"
        CancerHoroscopeDetail.horoscopeDescription = "Tenacious, loyal and sympathetic."
        CancerHoroscopeDetail.webSiteURL = "https://www.astrology.com/horoscope/daily/cancer.html"
        
        let LeoHoroscopeDetail = HoroscopeDetail()
        LeoHoroscopeDetail.horoscopeName = "leo"
        LeoHoroscopeDetail.fullImageName = "full-leo"
        LeoHoroscopeDetail.thumbImageName = "leo"
        LeoHoroscopeDetail.horoscopeSymbol = "Lion"
        LeoHoroscopeDetail.horoscopeMonth = "August"
        LeoHoroscopeDetail.horoscopeDescription = "Warm, action-oriented and driven by the desire to be loved and admired."
        LeoHoroscopeDetail.webSiteURL = "https://www.astrology.com/horoscope/daily/leo.html"
        
        let VirgoHoroscopeDetail = HoroscopeDetail()
        VirgoHoroscopeDetail.horoscopeName = "virgo"
        VirgoHoroscopeDetail.fullImageName = "full-virgo"
        VirgoHoroscopeDetail.thumbImageName = "virgo"
        VirgoHoroscopeDetail.horoscopeSymbol = "virgin"
        VirgoHoroscopeDetail.horoscopeMonth = "September"
        VirgoHoroscopeDetail.horoscopeDescription = "Methodical, meticulous, analytical and mentally astute."
        VirgoHoroscopeDetail.webSiteURL = "https://www.astrology.com/horoscope/daily/virgo.html"
        
        
        let LibraHoroscopeDetail = HoroscopeDetail()
        LibraHoroscopeDetail.horoscopeName = "libra"
        LibraHoroscopeDetail.fullImageName = "full-libra"
        LibraHoroscopeDetail.thumbImageName = "libra"
        LibraHoroscopeDetail.horoscopeSymbol = "Scales"
        LibraHoroscopeDetail.horoscopeMonth = "October"
        LibraHoroscopeDetail.horoscopeDescription = "Librans are famous for maintaining balance and harmony."
        LibraHoroscopeDetail.webSiteURL = "https://www.astrology.com/horoscope/daily/libra.html"
        
        let ScorpioHoroscopeDetail = HoroscopeDetail()
        ScorpioHoroscopeDetail.horoscopeName = "scorpio"
        ScorpioHoroscopeDetail.fullImageName = "full-scorpio"
        ScorpioHoroscopeDetail.thumbImageName = "scorpio"
        ScorpioHoroscopeDetail.horoscopeSymbol = "Scorpion"
        ScorpioHoroscopeDetail.horoscopeMonth = "November"
        ScorpioHoroscopeDetail.horoscopeDescription = "Strong willed and mysterious."
        ScorpioHoroscopeDetail.webSiteURL = "https://www.astrology.com/horoscope/daily/scorpio.html"
        
        let SagittariusHoroscopeDetail = HoroscopeDetail()
        SagittariusHoroscopeDetail.horoscopeName = "sagittarius"
        SagittariusHoroscopeDetail.fullImageName = "full-sagittarius"
        SagittariusHoroscopeDetail.thumbImageName = "sagittarius"
        SagittariusHoroscopeDetail.horoscopeSymbol = "Archer"
        SagittariusHoroscopeDetail.horoscopeMonth = "December"
        SagittariusHoroscopeDetail.horoscopeDescription = "Born adventurers."
        SagittariusHoroscopeDetail.webSiteURL = "https://www.astrology.com/horoscope/daily/sagittarius.html"
        
        let CapricornHoroscopeDetail = HoroscopeDetail()
        CapricornHoroscopeDetail.horoscopeName = "capricorn"
        CapricornHoroscopeDetail.fullImageName = "full-capricorn"
        CapricornHoroscopeDetail.thumbImageName = "capricorn"
        CapricornHoroscopeDetail.horoscopeSymbol = "Goat"
        CapricornHoroscopeDetail.horoscopeMonth = "January"
        CapricornHoroscopeDetail.horoscopeDescription = "The most determined sign in the Zodiac."
        CapricornHoroscopeDetail.webSiteURL = "https://www.astrology.com/horoscope/daily/capricorn.html"
        
        
        let AquariusHoroscopeDetail = HoroscopeDetail()
        AquariusHoroscopeDetail.horoscopeName = "Aquarius"
        AquariusHoroscopeDetail.fullImageName = "full-aquarius"
        AquariusHoroscopeDetail.thumbImageName = "aquarius"
        AquariusHoroscopeDetail.horoscopeSymbol = "Water Bearer"
        AquariusHoroscopeDetail.horoscopeMonth = "February"
        AquariusHoroscopeDetail.horoscopeDescription = "Humanitarians to the core."
        AquariusHoroscopeDetail.webSiteURL = "https://www.astrology.com/horoscope/daily/aquarius.html"
        
        let PiscesHoroscopeDetail = HoroscopeDetail()
        PiscesHoroscopeDetail.horoscopeName = "Pisces"
        PiscesHoroscopeDetail.fullImageName = "full-pisces"
        PiscesHoroscopeDetail.thumbImageName = "pisces"
        PiscesHoroscopeDetail.horoscopeSymbol = "Fish"
        PiscesHoroscopeDetail.horoscopeMonth = "March"
        PiscesHoroscopeDetail.horoscopeDescription = "Proverbial dreamers of the Zodiac."
        PiscesHoroscopeDetail.webSiteURL = "https://www.astrology.com/horoscope/daily/pisces.html"
        
        
        horoscopeDetails.append(AriesHoroscopeDetail)
        horoscopeDetails.append(TaurusHoroscopeDetail)
        horoscopeDetails.append(GeminiHoroscopeDetail)
        horoscopeDetails.append(CancerHoroscopeDetail)
        horoscopeDetails.append(LeoHoroscopeDetail)
        horoscopeDetails.append(VirgoHoroscopeDetail)
        horoscopeDetails.append(LibraHoroscopeDetail)
        horoscopeDetails.append(ScorpioHoroscopeDetail)
        horoscopeDetails.append(AquariusHoroscopeDetail)
        horoscopeDetails.append(CapricornHoroscopeDetail)
        horoscopeDetails.append(SagittariusHoroscopeDetail)
        horoscopeDetails.append(PiscesHoroscopeDetail)
    }

}
